import React from 'react';
import './App.css';
import { Formik, Form, Field, ErrorMessage } from 'formik';

const Basic = () => (
  <div className="bg">
    <Formik
      initialValues={{ name: '', email: '', phone: '', college: '', sem: '' }}
      validate={values => {
        let errors = {};
        if (!values.email) {
          errors.email = 'Required';
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = 'Invalid email address';
        }
        if (!values.name) {
          errors.name = "Required";
        }
        else if (values.name.length < 2 || /[0-9]/i.test(values.name)) {
          errors.name = "Invalid name";
        }
        if (!values.phone) {
          errors.phone = 'Required';
        }
        else {
          let s = String(values.phone);
          if (s.length < 10) {
            errors.phone = 'Invalid phone no';
          }
        }
        if (!values.college) {
          errors.college = "Required";
        }
        else if (values.college.length < 2||/[0-9]/i.test(values.name)) {
          errors.college = "Invalid college name";
        }
        if (!values.sem) {
          errors.sem = "Required";
        }
        else if (values.sem < 1 || values.sem > 8) {
          errors.sem = "Invalid semester (should be between 1 to 8)";
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
    >
      {({ isSubmitting }) => (
        <Form className="registration">
          <p className="heading">Registration</p>
          <Field className="inputFeild" type="text" name="name" placeholder="Enter your name" />
          <div className="lineBreaker">
            <ErrorMessage className="error" name="name" component="span" />
          </div>
          <Field className="inputFeild" type="email" name="email" placeholder="Enter your email id" />
          <div className="lineBreaker">
            <ErrorMessage className="error" name="email" component="span" />
          </div>
          <Field className="inputFeild" type="number" name="phone" placeholder="Enter your phone number" />
          <div className="lineBreaker">
            <ErrorMessage className="error" name="phone" component="span" />
          </div>
          <Field className="inputFeild" type="text" name="college" placeholder="Enter your college name" />
          <div className="lineBreaker">
            <ErrorMessage className="error" name="college" component="span" />
          </div>
          <Field className="inputFeild" type="number" name="sem" placeholder="Enter your semester" />
          <div className="lineBreaker">
            <ErrorMessage className="error" name="sem" component="span" />
          </div>
          <button className="submitButton" type="submit" disabled={isSubmitting}>
            Submit
          </button>
        </Form>
      )}
    </Formik>
  </div>
);

export default Basic;